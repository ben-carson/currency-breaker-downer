import locale
from decimal import *

locale.setlocale(locale.LC_ALL, '' )

def find_num_of_fifties(input):
       fifties = input / 50
       return int(fifties)

def find_num_of_twenties(input):
       twenties = input / 20
       return int(twenties)

def find_num_of_tens(input):
       tens = input / 10
       return int(tens)

def find_num_of_fives(input):
       fives = input / 5
       return int(fives)

def find_num_of_ones(input):
       ones = input / 1
       return int(ones)

def find_num_of_quarters(input):
       quarters = input / Decimal('.25')
       return int(quarters)

def find_num_of_dimes(input):
       dimes = input / Decimal('.10')
       return int(dimes)

def find_num_of_nickels(input):
       nickels = Decimal(input) / Decimal('.05')
       return int(nickels)

def find_num_of_pennies(input):
       pennies = input / Decimal('.01')
       return int(pennies)

def convert_to_money(input):
       return (Decimal(input)).quantize(Decimal('.01'), rounding = ROUND_HALF_UP)


print('Welcome. Let\'s begin.')
raw_total = input("What is the currency amount you'd like to breakdown?\n")
'''
numeric_total is the running total that will actually have
computations performed against it
'''
numeric_total = (Decimal(raw_total)).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
print ("You entered: %s" % locale.currency(numeric_total, grouping=True))

print ("--== THE BREAKDOWN ==--")
the_fifties = find_num_of_fifties(numeric_total)
fifties_total_amount = convert_to_money(the_fifties * 50)
fifties_total_amount_str = locale.currency(fifties_total_amount, grouping=True)
numeric_total -= fifties_total_amount

the_twenties = find_num_of_twenties(numeric_total)
twenties_total_amount = convert_to_money(the_twenties * 20)
twenties_total_amount_str = locale.currency(twenties_total_amount, grouping=True)
numeric_total -= twenties_total_amount

the_tens = find_num_of_tens(numeric_total)
tens_total_amount = convert_to_money(the_tens * 10)
tens_total_amount_str = locale.currency(tens_total_amount, grouping=True)
numeric_total -= tens_total_amount

the_fives = find_num_of_fives(numeric_total)
fives_total_amount = convert_to_money(the_fives * 5)
fives_total_amount_str = locale.currency(fives_total_amount, grouping=True)
numeric_total -= fives_total_amount

the_ones = find_num_of_ones(numeric_total)
ones_total_amount = convert_to_money(the_ones * 1)
ones_total_amount_str = locale.currency(ones_total_amount, grouping=True)
numeric_total -= ones_total_amount

the_quarters = find_num_of_quarters(numeric_total)
quarters_total_amount = convert_to_money(the_quarters * Decimal('.25'))
quarters_total_amount_str = locale.currency(quarters_total_amount, grouping=True)
numeric_total -= quarters_total_amount

the_dimes = find_num_of_dimes(numeric_total)
dimes_total_amount = convert_to_money(the_dimes * Decimal('.10'))
dimes_total_amount_str = locale.currency(dimes_total_amount, grouping=True)
numeric_total -= dimes_total_amount

the_nickels = find_num_of_nickels(numeric_total)
nickels_total_amount = convert_to_money(the_nickels * Decimal('.05'))
nickels_total_amount_str = locale.currency(nickels_total_amount, grouping=True)
numeric_total -= nickels_total_amount

the_pennies = find_num_of_pennies(numeric_total)
pennies_total_amount = convert_to_money(the_pennies * Decimal('.01'))
pennies_total_amount_str = locale.currency(pennies_total_amount, grouping=True)
numeric_total -= pennies_total_amount

if(numeric_total == 0.0):
    print("$50  x %r = %s" % (the_fifties, fifties_total_amount_str))
    print("$20  x %r = %s" % (the_twenties, twenties_total_amount_str))
    print("$10  x %r = %s" % (the_tens, tens_total_amount_str))
    print("$5   x %r = %s" % (the_fives, fives_total_amount_str))
    print("$1   x %r = %s" % (the_ones, ones_total_amount_str))
    print("$.25 x %r = %s" % (the_quarters, quarters_total_amount_str))
    print("$.10 x %r = %s" % (the_dimes, dimes_total_amount_str))
    print("$.05 x %r = %s" % (the_nickels, nickels_total_amount_str))
    print("$.01 x %r = %s" % (the_pennies, pennies_total_amount_str))
else:
    print("There is " + str(numeric_total) + " left over! You done messed up!")
    print("Bad programmer!")
    print("Bad!")
