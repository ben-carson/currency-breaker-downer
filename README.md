currency-breaker-downer
=======================

This is a very simple script to compute the number & types of bills & coins (USD) needed to reach a total amount. This is part of my Python learning experience, as well as useful for the envelope app I'd like to build.
